/*
 * @Author: your name
 * @Date: 2020-10-10 11:30:21
 * @LastEditTime: 2020-11-02 18:33:10
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \网易云移动端\js\hotsearch.js
 */
//生成热门搜索
$(function () {
  $.get('http://139.9.209.237:3000/search/hot', function (res) {
    res.result.hots.forEach(function (item, index) {
      var $div = $("<div class='btn btn-default'></div>").html(item.first);
      $div.on('click', function () {
        loadSongList(item.first);
        storagekeyword(item.first);
      });
      $('.search-hot-body').append($div);
    });
  });
  //根据关键字进行搜索

  var KEY_HISTORY_LIST = 'HISTORY_LIST';
  //4-1 加载热门搜索

  //4-2 加载本地关键字
  loadLocalHistories();
  //4-3 输入内容、搜索
  $('#search-txt').on('input', function (e) {
    var KEY_HISTORY_LIST = 'HISTORY_LIST';
    var keyword = this.value;
    //存储关键字
    loadSongList(keyword);
    $('#clearAll').addClass('icon-qingchu');
    $('.icon-qingchu').on('click', function () {
      $('#search-txt').val('');
      keyword = '';
      updatehistorieswrap();
    });
    if (keyword.length == 0) {
      updatehistorieswrap();
    }
    if (e.keyCode == 13 && keyword.length > 0) {
      storagekeyword(keyword);
      //更新界面
      loadSongList(keyword);
    }
  });

  $('#search-txt').on('focus', function (e) {
    if ($('#search-txt').val().length > 0) {
      $('#clearAll').addClass('icon-qingchu');
      loadSongList($('#search-txt').val());
    }
  });
  //存储关键字
  function storagekeyword(keyword) {
    var histories = [];
    if (JSON.parse(localStorage.getItem(KEY_HISTORY_LIST))) {
      histories = JSON.parse(localStorage.getItem(KEY_HISTORY_LIST));
      var index = histories.indexOf(keyword);
      if (index >= 0) {
        histories.splice(index, 1);
      }
    }
    histories.unshift(keyword);
    localStorage.setItem(KEY_HISTORY_LIST, JSON.stringify(histories));
  }

  //加载搜索歌曲列表
  function loadSongList(keyword) {
    $.get('http://139.9.209.237:3000/search/get?keywords=' + keyword, function (
      res
    ) {
      if (res.code == 200) {
        $('.search-song-list').show();
        $('.search-hot-title').hide();
        $('.search-hot-body').hide();
        $('.search-history').hide();
        $('.search-song-list').html('');
        res.result.songs.forEach(function (item, index) {
          var $li = $(
            ` <li>
              <a href="#">
                <div class="search-song-left">
                  <img
                    src=""
                  />
                </div>
                <div class="search-song-right">
                  <h5>` +
              item.name +
              `</h5>
                  <h6>` +
              item.artists[0].name +
              `</h6>
                </div>
              </a>
            </li>`
          );
          $li.prop('data-id', item.id);
          $.get(
            'http://139.9.209.237:3000/song/detail',
            { ids: $li.prop('data-id') },
            function (res) {
              $li.find('img').attr('src', res.songs[0].al.picUrl);
            }
          );
          $li.appendTo($('.search-song-list'));
          $li.on('click', function () {
            var id = $li.prop('data-id');
            $.get(
              'http://139.9.209.237:3000/song/detail?ids=' +
                $(this).prop('data-id'),
              function (res) {
                if (res.code == 200) {
                  $.get(
                    'http://139.9.209.237:3000/song/url?id=' + id,
                    function (res) {
                      $('audio').attr('src', res.data[0].url);
                      $('audio')[0].play();
                      //加载播放
                      $('audio').on('loadedmetadata', function (e) {
                        var minute = Math.floor(e.target.duration / 60);
                        minute = minute < 10 ? '0' + minute : minute;
                        var second = Math.floor(e.target.duration % 60);
                        second = second < 10 ? '0' + second : second;
                        $('.total-time').text(minute + ':' + second);
                      });
                    }
                  );
                  $('.disc-content img')
                    .attr('src', res.songs[0].al.picUrl)
                    .prop('data-Id', id);
                  $('.playing img').attr('src', res.songs[0].al.picUrl);
                  $('.play-title h5').text(res.songs[0].name);
                  $('.play-title h6').text(res.songs[0].ar[0].name);
                  $('.playing-song h5').text(res.songs[0].name);
                  $('.playing-song h6').text(res.songs[0].ar[0].name);
                  $('.playstate')
                    .removeClass('icon-bofang_huaban')
                    .addClass('icon-bofang');
                  $('.disc-content img')
                    .removeClass('pause')
                    .addClass('rotate');
                  $('.playing img').addClass('rotate').removeClass('pause');
                  $('audio')[0].play();
                }
              }
            );
          });
        });
        $out = $(`<li id="song-list-out">
              <span class="iconfont icon-cuo"></span>
            </li>`);
        $('.search-song-list').prepend($out);
        $out.find('.icon-cuo').on('click', function () {
          //退出歌曲面板，进入热门搜索和历史搜索面板
          updatehistorieswrap();
        });
      }
    });
  }
  function loadLocalHistories() {
    if (JSON.parse(localStorage.getItem(KEY_HISTORY_LIST))) {
      var historieslist = JSON.parse(localStorage.getItem(KEY_HISTORY_LIST));
      $('.search-history').html('');
      historieslist.forEach(function (item, index) {
        var $li = createSearchHistory(item);
        $li.appendTo($('.search-history'));
      });
    }
  }

  //4-4 删除图标
  //更新界面
  function updatehistorieswrap() {
    loadLocalHistories();
    $('.search-song-list').hide();
    $('.search-hot-title').show();
    $('.search-hot-body').show();
    $('.search-history').show();
    $('#clearAll').removeClass('icon-qingchu');
  }
  //创建历史搜索
  function createSearchHistory(txt) {
    $li = $(`<li>
              <div class="iconfont icon-yinle"></div>
            </li>`);
    $div = $(
      `<div class="keyword">` +
        txt +
        `</div><div class="iconfont icon-cuo"></div>`
    );
    $li.append($div);
    $li.find('.icon-cuo').on('click', function () {
      var histories = [];
      //删除本地存储数据0
      histories = JSON.parse(localStorage.getItem(KEY_HISTORY_LIST));
      var index = histories.indexOf(txt);
      histories.splice(index, 1);
      localStorage.setItem(KEY_HISTORY_LIST, JSON.stringify(histories));
      //更新界面
      $(this).parent().remove();
    });
    $li.find('.keyword').on('click', function () {
      loadSongList(txt);
    });
    return $li;
  }
});
