/*
 * @Author: your name
 * @Date: 2020-10-10 10:06:23
 * @LastEditTime: 2020-10-10 16:23:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \网易云移动端\js\banner.js
 */
//加载轮播图
function loadBanners() {
  $.get("http://139.9.209.237:3000/banner", function (res) {
    res.banners.forEach(function (item, index) {
      var $div = $('<div class="swiper-slide"></div>');
      var $img = $("<img />").attr("src", item.imageUrl);
      $div.append($img);
      $(".swiper-wrapper").append($div);
    });
    //轮播插件参数设置
  }).then(function () {
    var mySwiper = new Swiper(".body-banner", {
      direction: "horizontal", // 水平切换选项
      loop: true, // 循环模式选项
      autoplay: {
        disableOnInteraction: false,
      },
      speed: 1000,
    });
  });
}
