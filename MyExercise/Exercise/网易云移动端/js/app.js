/*
 * @Author: your name
 * @Date: 2020-10-10 08:39:06
 * @LastEditTime: 2020-10-13 00:54:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \网易云移动端\js\app.js
 */
$(function () {
  $(".flash-page .btn").on("click", function () {
    clearTimeout(timer);
    $(".flash-page").fadeOut(2000);
    $(".home-page").css("display", "flex");
    loadBanners();
  });

  var timer = setTimeout(function () {
    $(".flash-page").fadeOut(3000);
    $(".home-page").css("display", "flex");
    clearTimeout(timer);
    loadBanners();
  }, 3000);

  // loadBanners();
  //加载轮播图
  function loadBanners() {
    //加载轮播图
    $.get("http://139.9.209.237:3000/banner", function (res) {
      res.banners.forEach(function (item, index) {
        var $div = $('<div class="swiper-slide"></div>');
        var $img = $("<img />").attr("src", item.imageUrl);
        $div.append($img);
        $(".swiper-wrapper").append($div);
      });
      //轮播插件参数设置
    }).then(function () {
      var mySwiper = new Swiper(".body-banner", {
        direction: "horizontal", // 水平切换选项
        loop: true, // 循环模式选项
        autoplay: {
          disableOnInteraction: false,
        },
        speed: 1000,
      });
    });
  }
});
//点击导航切换
$(function () {
  $(".home-page-nav a").on("click", function (e) {
    var prev = $(".active");
    prev.removeClass("active");
    $(this).addClass("active");
    var index = $(this).parent().index();
    $(".home-page-body>div").hide();
    $(".home-page-body>div").eq(index).show();
  });
});
$(function () {
  $(".home-page-footer .playing").on("click", function () {
    $(".play-page").css("display", "flex");
    $(".home-page").slideUp(1000, function () {
      $(".home-page").hide();
    });
  });
});
//点击切换播放状态
$(function () {
  $(".playstate").on("click", function () {
    if (this.classList.contains("icon-bofang")) {
      $(".playstate").removeClass("icon-bofang");
      $(".playstate").addClass("icon-bofang_huaban");
      $(".disc-content img").addClass("pause");
      $(".playing img").addClass("pause");
      $("audio")[0].pause();
    } else {
      $(".playstate").removeClass("icon-bofang_huaban");
      $(".playstate").addClass("icon-bofang");
      $(".disc-content img").removeClass("pause");
      $(".disc-content img").addClass("rotate");
      $(".playing img").addClass("rotate");
      $(".playing img").removeClass("pause");
      $("audio")[0].play();
    }
  });
});
