/*
 * @Author: your name
 * @Date: 2020-10-10 16:08:27
 * @LastEditTime: 2020-10-13 01:29:13
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \网易云移动端\js\play.js
 */
$(function () {
  $(".play-page .icon-xia").on("click", function () {
    $(".home-page").slideDown(1000, function () {
      loadBanners();
      $(".home-page").css("display", "flex");
      $(".play-page").hide();
    });
  });

  //加载播放
  $("audio").on("loadedmetadata", function (e) {
    var minute = Math.floor(e.target.duration / 60);
    minute = minute < 10 ? "0" + minute : minute;
    var second = Math.floor(e.target.duration % 60);
    second = second < 10 ? "0" + second : second;
    $(".total-time").text(minute + ":" + second);
  });

  //更新时间
  $("audio").on("timeupdate", function (e) {
    var timepercent = e.target.currentTime / e.target.duration;
    var barwidth = $(".progress-bar").width();
    var currentposition = timepercent * barwidth;
    $(".bar").css("width", currentposition);
    var minute = Math.floor(e.target.currentTime / 60);
    minute = minute < 10 ? "0" + minute : minute;
    var second = Math.floor(e.target.currentTime % 60);
    second = second < 10 ? "0" + second : second;
    $(".start-time").text(minute + ":" + second);
  });
  //拖拽双向绑定
  //-1-
  var point = $(".point")[0];
  var progress = $(".progress-bar")[0];
  var move = false;
  // var progressoffsetleft = $(".progress-bar").offset().left;
  var progressoffsetleft=65;
  var progresswidth = $(".progress-bar").width();
  progress.ontouchstart = function (e) {
    move = true;
    var barwidth = e.touches[0].clientX - progressoffsetleft;
    $(".bar")[0].style.width = barwidth + "px";
    var currenttimelength = (barwidth / progresswidth) * ($("audio")[0].duration);
    $("audio")[0].currentTime = currenttimelength;
  };
  //-2-
  progress.ontouchmove = function (e) {
    if (move) {
      var barwidth = e.touches[0].clientX - progressoffsetleft;
      if (barwidth <= progresswidth) {
        $(".bar")[0].style.width = barwidth + "px";
        var currenttimelength =
          (barwidth / progresswidth) * $("audio")[0].duration;
        $("audio")[0].currentTime = currenttimelength;
      }
    }
  };

  //-3-
  progress.ontouchend = function (e) {
    move = false;
  };
  //上一首点击
  $(".icon-shangyishou").on("click", function () {
    if (flag == 2) {
      randomplaymodel();
      return;
    }
    var $li;
    $(".search-song-list li").each(function (index, item) {
      if ($(".disc-content img").prop("data-Id") == $(item).prop("data-id")) {
        $li = $(item);
      }
    });
    var id;
    if ($li.index() == 1) {
      id = $(".search-song-list li:last-child").prop("data-id");
    } else {
      id = $li.prev().prop("data-id");
    }
    changeSong(id);
  });
  //下一首点击
  var flag;
  $(".icon-xiayishou").on("click", function () {
    if (flag == 2) {
      randomplaymodel();
      return;
    }
    var $li;
    $(".search-song-list li").each(function (index, item) {
      if ($(".disc-content img").prop("data-Id") == $(item).prop("data-id")) {
        $li = $(item);
      }
    });
    var id;
    if ($li.index() == $(".search-song-list li").length - 1) {
      id = $(".search-song-list li:nth-child(2)").prop("data-id");
    } else {
      id = $li.next().prop("data-id");
    }
    changeSong(id);
  });
  function songloopmodel() {
    $("audio")[0].onended = function () {
      $(this).attr("loop", true);
    };
  }
  function randomplaymodel() {
      var len = $(".search-song-list li").length - 1;
      var random = Math.floor(Math.random() * len) + 1;
      var $li = $(".search-song-list li").eq(random);
      changeSong($li.prop("data-id"));
  }
  //单曲循环
  $(".playmodel").on("click", function (e) {
    if (e.target.classList[2] == "icon-xunhuanbofang") {
      flag = 1;
      $(this).addClass("icon-danquxunhuan").removeClass("icon-xunhuanbofang");
      songloopmodel();
      return;
    }
    //随机播放
    if (e.target.classList[2] == "icon-danquxunhuan") {
      flag = 2;
      $("audio").attr("loop", false);
      $(this).addClass("icon-ziyuan").removeClass("icon-danquxunhuan");
      $("audio")[0].onended = function () {
        randomplaymodel();
      };
      return;
    }
    //循环播放
    if (e.target.classList[2] == "icon-ziyuan") {
      flag = 3;
      $(this).addClass("icon-xunhuanbofang").removeClass("icon-ziyuan");
      $("audio")[0].onended = function () {
        var $li;
        $(".search-song-list li").each(function (index, item) {
          if (
            $(".disc-content img").prop("data-Id") == $(item).prop("data-id")
          ) {
            $li = $(item);
          }
        });
        var id;
        if ($li.index() == $(".search-song-list li").length - 1) {
          id = $(".search-song-list li:nth-child(2)").prop("data-id");
        } else {
          id = $li.next().prop("data-id");
        }
        changeSong(id);
      };
    }
  });

  //切换歌曲
  function changeSong(id) {
    $.get("http://139.9.209.237:3000/song/detail?ids=" + id, function (res) {
      if (res.code == 200) {
        $(".playing img").attr("src", res.songs[0].al.picUrl);
        $.get("http://139.9.209.237:3000/song/url?id=" + id, function (res) {
          $("audio").attr("src", res.data[0].url);
          $("audio")[0].play();
          //加载播放
          $("audio").on("loadedmetadata", function (e) {
            var minute = Math.floor(e.target.duration / 60);
            minute = minute < 10 ? "0" + minute : minute;
            var second = Math.floor(e.target.duration % 60);
            second = second < 10 ? "0" + second : second;
            $(".total-time").text(minute + ":" + second);
          });
        });
        $(".disc-content img")
          .attr("src", res.songs[0].al.picUrl)
          .prop("data-Id", id);
        $(".play-title h5").text(res.songs[0].name);
        $(".play-title h6").text(res.songs[0].ar[0].name);
        $(".playing-song h5").text(res.songs[0].name);
        $(".playing-song h6").text(res.songs[0].ar[0].name);
        $(".playstate")
          .removeClass("icon-bofang_huaban")
          .addClass("icon-bofang");
        $(".disc-content img").removeClass("pause").addClass("rotate");
        $(".playing img").addClass("rotate").removeClass("pause");
        $("audio")[0].play();
        $('.icon-fuwutuijian').css('color','#E0E0E0')
      }
    });
  }
  //加载轮播图
  function loadBanners() {
    $.get("http://139.9.209.237:3000/banner", function (res) {
      res.banners.forEach(function (item, index) {
        var $div = $('<div class="swiper-slide"></div>');
        var $img = $("<img />").attr("src", item.imageUrl);
        $div.append($img);
        $(".swiper-wrapper").append($div);
      });
      //轮播插件参数设置
    }).then(function () {
      var mySwiper = new Swiper(".body-banner", {
        direction: "horizontal", // 水平切换选项
        loop: true, // 循环模式选项
        autoplay: {
          disableOnInteraction: false,
        },
        speed: 1000,
      });
    });
  }
});

$(function () {
  $(".play-page-footer .icon-fuwutuijian").on("click", function () {
    if ($(this).css("color") == "rgb(224, 224, 224)") {
      $(this).css("color", "red");
    } else {
      $(this).css("color", "#e0e0e0");
    }
  });
});
