/*
 * @Author: your name
 * @Date: 2020-10-10 10:29:47
 * @LastEditTime: 2020-10-10 13:49:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \网易云移动端\js\recommend.js
 */
$(function () {
  $.get("http://139.9.209.237:3000/personalized", function (res) {
    console.log(res);
    res.result.forEach(function (item, index) {
      var $li = createRecommendLi(item);
      $(".recommend-list").append($li);
    });
  });

  function createRecommendLi(item) {
    var $li = $("<li><a></a></li>");
    var $img = $("<img />");
    $img.attr("src", item.picUrl);
    $li.append($img);
    return $li;
  }
});
