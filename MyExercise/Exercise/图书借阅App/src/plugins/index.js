import Vue from 'vue';
import {
  Button,
  Message,
  Form,
  Input,
  Icon,
  Checkbox,
  Layout,
  Slider,
  Menu,
  Avatar,
  PageHeader,
  Descriptions,
  Tag,
  Row,
  Statistic,
  Col,
  Card,
  Table,
  Modal,
  Popconfirm,
  Select,
  Pagination,
  Tabs,
  FormModel,
  Switch,
  DatePicker,
  Radio,
  Upload,
  Tooltip,
} from 'ant-design-vue';

Vue.prototype.$confirm = Modal.confirm;
Vue.prototype.$message = Message;

Vue.use(Avatar);
Vue.use(Tooltip);
Vue.use(Upload);
Vue.use(Radio);
Vue.use(DatePicker);
Vue.use(Switch);
Vue.use(FormModel);
Vue.use(Pagination);
Vue.use(Tabs);
Vue.use(Select);
Vue.use(Popconfirm);
Vue.use(Modal);
Vue.use(Table);
Vue.use(Card);
Vue.use(Col);
Vue.use(Statistic);
Vue.use(Row);
Vue.use(Tag);
Vue.use(Descriptions);
Vue.use(PageHeader);
Vue.use(Button);
Vue.use(Layout);
Vue.use(Slider);
Vue.use(Menu);
Vue.use(Input);
Vue.use(Icon);
Vue.use(Form);
Vue.use(Checkbox);
Vue.use(Message);
