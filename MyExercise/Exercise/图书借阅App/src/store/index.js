import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    user: {
      phone: '',
      password: '',
      readerId: '',
    },
  },
  getters: {},
  mutations: {
    setUser(state, userInfo = {}) {
      state.user = userInfo;
      localStorage.setItem('readerId', JSON.stringify(state.user.readerId));
    },
  },
  actions: {},
});
