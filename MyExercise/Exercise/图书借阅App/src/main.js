import Vue from 'vue';
import store from './store';
import router from './router';
import 'ant-design-vue';
import './plugins';
import './assets/fonts/iconfont.css';
import App from './App.vue';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
