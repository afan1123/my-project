import axios from 'axios';

axios.defaults.baseURL = 'http://139.9.209.237/librarywebapi/';

// 添加请求拦截器
axios.interceptors.request.use(
  function(config) {
    // 在发送请求之前做些什么
    return config;
  },
  function(error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  function(response) {
    // 对响应数据做点什么
    let res = response.data;
    if (res.Code == 100) {
      return res;
    }
  },
  function(error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);
function buildFormData(book) {
  let fd = new FormData();
  for (let [key, value] of Object.entries(book)) {
    fd.append(key, value);
  }
  return fd;
}
export default {
  login(phone, password) {
    return axios.post('member/login', { phone, password });
  },
  getCategory() {
    return axios.get('category/list');
  },
  createCategory(name, icon = null) {
    return axios.post('category/create', { name, icon });
  },
  categoryUpdate(id, name, icon = null) {
    return axios.post('category/update', { id, name, icon });
  },
  getMyShelf(readerId) {
    return axios.get('Transaction/GetMyShelf', { params: { readerId } });
  },
  getCategoryList() {
    return axios.get('category/list');
  },
  getPublisherList() {
    return axios.get('publisher/list');
  },
  getBookList(categoryId = '', publisherId = '', keyword = '') {
    return axios.get('book/list', {
      params: { categoryId, publisherId, keyword },
    });
  },
  getBooksBySection(sectionId) {
    return axios.get('book/GetBooksBySection', { params: { sectionId } });
  },
  getAuthorList() {
    return axios.get('author/list');
  },
  createBook(book) {
    console.log('create:', book);
    let bookFd = buildFormData(book);
    return axios.post('book/create', bookFd);
  },
  putawayBook(bookId, count, libraryId = '') {
    return axios.post('book/putaway', { bookId, count, libraryId });
  },
  updateBook(book) {
    console.log('update:', book);
    let bookFd = buildFormData(book);
    return axios.post('book/update', bookFd);
  },
  getBorrowRecords(readerId) {
    return axios.post('Transaction/GetBorrowRecords', { readerId });
  },
  getAllBorrowRecords() {
    return axios.get('Transaction/GetAllBorrowRecords');
  },
  //取消借阅
  cancelOrder(orderId, readerId) {
    return axios.post('Transaction/CancelOrder', { orderId, readerId });
  },
  //配送
  distribution(orderId) {
    return axios.get('Transaction/Distribution', { params: { orderId } });
  },
  //确认收货
  confirmOrder(orderId, readerId) {
    return axios.post('Transaction/ConfirmOrder', { orderId, readerId });
  },
  //归还
  returnBook(orderId, bookNumber) {
    console.log(orderId, bookNumber);
    return axios.get('Transaction/ReturnBook', {
      params: { orderId, bookNumber },
    });
  },
};
