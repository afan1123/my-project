import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import WelcomePage from '../pages/WelcomePage.vue';
import MainPage from '../pages/MainPage.vue';
import LoginPage from '../pages/LoginPage.vue';
import CategoryPage from '../pages/CategoryPage.vue';
import BorrowPage from '../pages/BorrowPage.vue';
import BookinfoPage from '../pages/BookinfoPage.vue';
import DataPage from '../pages/DataPage.vue';
import ConfigPage from '../pages/ConfigPage.vue';
import EditPage from '../pages/EditPage.vue';

//import HelloWorld from '@/components/HelloWorld'
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};
const routes = [
  { path: '/', redirect: 'main' },
  { path: '/login', name: 'login', component: LoginPage },
  {
    path: '/main',
    component: MainPage,
    children: [
      { path: '', redirect: 'welcome' },
      { path: 'welcome', name: 'welcome', component: WelcomePage },
      { path: 'category', name: 'category', component: CategoryPage },
      { path: 'data', name: 'data', component: DataPage },
      { path: 'borrow', name: 'borrow', component: BorrowPage },
      { path: 'bookinfo', name: 'bookinfo', component: BookinfoPage },
      { path: 'config', name: 'config', component: ConfigPage },
      { path: 'edit/:id?', name: 'edit', component: EditPage },
    ],
  },
];
export default new VueRouter({ routes });
