/*
 * @Author: your name
 * @Date: 2020-09-24 09:14:48
 * @LastEditTime: 2020-09-29 13:39:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \图书管理系统\js\login.js
 */
$(function () {
  $("button").on("click", function (e) {
    e.preventDefault();
    var phone = $("#phone").val();
    var psw = $("#password").val();
    var pattern = /^1[35789]\d{9}$/;
    if (!pattern.test(phone)) {
      $("#prompt").text("手机号码格式错误");
      return false;
    } else {
      var data = {
        phone: phone,
        password: psw,
      };
    }
    $.post(
      "http://139.9.209.237/htmlprojectwebapi/account/login",
      data,
      function (res) {
        if (res.Code == 100) {
          toastr.options.showMethod = "slideDown";
          toastr.options.hideMethod = "slideUp";
          toastr.options.onHidden = function () {
            $("#prompt").text("");
            sessionStorage.setItem("user", JSON.stringify(res.Data));
            sessionStorage.setItem("userInfo", JSON.stringify(data));
            $("#phone").val("");
            $("#password").val("");
            location.href = "index.html";
          };
          toastr.success("成功登陆", "恭喜您！");
        }
      }
    );
  });
});
