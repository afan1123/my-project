/*
 * @Author: your name
 * @Date: 2020-09-24 10:35:54
 * @LastEditTime: 2020-09-29 10:48:51
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \图书管理系统\js\System.js
 */
$(function () {
  var pattern = /^\d{4}$/;
  $("#ok").on("click", function (e) {
    e.preventDefault();
    var userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    var user=JSON.parse(sessionStorage.getItem("user"));
    if (!(userInfo.password == $("#prevPsw").val())) {
      $("#prevPsw").attr("data-content", "原密码错误，请重新输入！");
      $("#prevPsw").popover("show");
      return;
    }
    $("#prevPsw").popover("hide");
    if (!pattern.test($("#newPsw").val())) {
      $("#newPsw").attr(
        "data-content",
        "新密码格式不正确，由4位数字组成"
      );
      $("#newPsw").popover("show");
      return;
    }
    $("#newPsw").popover("hide");
    if ($("#newPsw").val() != $("#newConfirm").val()) {
      $("#newConfirm").attr("data-content", "密码输入不一致！");
      $("#newConfirm").popover("show");
      return;
    }
    $("#newConfirm").popover("hide");
    var MEMBER_URL_RESET = "http://139.9.209.237/htmlprojectwebapi/account/modifypassword";
    var obj={
      id:user.Id,
      oldPassword:$('#prevPsw').val(),
      newPassword:$("#newPsw").val()
    }
    $.post(MEMBER_URL_RESET,obj).then(function(res){
      if(res.Code==100){
        location.href="login.html";
      }
    });
  });
});
