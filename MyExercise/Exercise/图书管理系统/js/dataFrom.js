/*
 * @Author: your name
 * @Date: 2020-09-28 14:43:22
 * @LastEditTime: 2020-09-29 10:31:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \图书管理系统\js\dataFrom.js
 */
$(function () {
  var DATA_URL_LIST = "http://139.9.209.237/librarywebapi/Statistic/list";
  $.get(DATA_URL_LIST, function (res) {
    var barArrX = [];
    var barArrY = [];
    var pieName = [];
    var pieSide = [];
    res.Data.forEach(function (item) {
      barArrX.push(item.Name);
      barArrY.push(item.Count);
      pieName.push(item.Name);
      pieSide.push({ value: item.Count, name: item.Name });
    });
    var obj1 = {
      title: "图书管理系统",
      arrX: barArrX,
      arrY: barArrY,
    };
    var obj2={
        pieName:pieName,
        pieSide:pieSide,
        title:"图书管理系统"
    }
    loadBar(obj1);
    loadPie(obj2)
  });

  function loadPie(obj) {
    var myChart = echarts.init(document.getElementById("main2"));

    option = {
      title: {
        text: obj.title,
        left: "center",
      },
      tooltip: {
        trigger: "item",
        formatter: "{a} <br/>{b} : {c} ({d}%)",
      },
      legend: {
        left: "center",
        top:30,
        data: obj.pieName,
      },
      series: [
        {
          name: "访问来源",
          type: "pie",
          radius: "55%",
          center: ["50%", "60%"],
          data: obj.pieSide,
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: "rgba(0, 0, 0, 0.5)",
            },
          },
        },
      ],
    };
    myChart.setOption(option);
  }
  function loadBar(obj) {
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById("main1"));

    // 指定图表的配置项和数据
    var option = {
      title: {
        text: obj.title,
        left:"center"
      },
      tooltip: {},
      legend: {
        data: ["销量"],
        left:"right"
      },
      xAxis: {
        data: obj.arrX,
        axisLabel: {
          rotate: 45,
          interval: 0,
        },
      },
      yAxis: {},
      series: [
        {
          name: "销量",
          type: "bar",
          data: obj.arrY,
        },
      ],
    };

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
  }
});
