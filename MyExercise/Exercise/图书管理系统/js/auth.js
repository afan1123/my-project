/*
 * @Author: your name
 * @Date: 2020-09-24 12:14:28
 * @LastEditTime: 2020-09-26 14:56:10
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \图书管理系统\js\auth.js
 */
window.onload = function () {
  var names = document.querySelectorAll(".name");
  var user =JSON.parse(sessionStorage.getItem("user"));
  if (user) {
    names.forEach(function (item, index) {
      item.innerText=user.TrueName;
    });
    return;
  }
  location.href = "login.html";
};
