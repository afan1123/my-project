/*
 * @Author: your name
 * @Date: 2020-09-24 08:08:27
 * @LastEditTime: 2020-09-29 13:39:37
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \图书管理系统\js\categroyManger.js
 */
$(function () {
  $.get("http://139.9.209.237/librarywebapi/category/list", function (res) {
    res.Data.forEach(function (item, index) {
      $("tbody").append(loadTr(item, index));
    });
  }).then(edit);
});

//增加图书
$(function () {
  $("#sure").on("click", function () {
    var newBook = {
      name: $("#newName").val(),
      weight: $("#newWeight").val(),
    };
    $.post(
      "http://139.9.209.237/librarywebapi/category/create?name=" + newBook.name,
      function (res) {
            //重新加载
        if (res.Code == 100) {
          $("tbody").html("");
          $.get("http://139.9.209.237/librarywebapi/category/list", function (
            res1
          ) {
            res1.Data.forEach(function (item, index) {
              $("tbody").append(loadTr(item, index));
            });
          }).then(edit);
        }
      }
    );
    $("#addBook").modal("hide");
  });
});
//搜索
$(function () {
  $(".fa-search").on("click", function () {
      console.log(1);
    var content = $("#search").val();
    var arr=[];
    $.get("http://139.9.209.237/librarywebapi/category/list",function(res){
        arr=res.Data;
        return arr;
    })
  });
});
//编辑操作
function edit() {
  $("tbody tr").on("click", function (e) {
    if (e.target.tagName == "SPAN") {
      var weight = $(e.target).parent().prev().text();
      var name = $(e.target).parent().prev().prev().text();
      $("#name").val(name);
      $("#weight").val(weight);
      $("#save").on("click", function () {
        $(e.target).parent().prev().text($("#weight").val());
        $(e.target).parent().prev().prev().text($("#name").val());
        $("#editor").modal("hide");
        return;
      });
    } else {
      return;
    }
  });
}
//加载行元素
function loadTr(item, index) {
  var $tr = $(
    `<tr><td>` +
      (index + 1) +
      `</td> <td>` +
      item.Name +
      `</td><td>` +
      item.Priority +
      `</td>
        <td><span title="编辑状态" data-toggle="modal" data-target="#editor" class="fa fa-edit"></span></td>      </tr>`
  );
  return $tr;
}
