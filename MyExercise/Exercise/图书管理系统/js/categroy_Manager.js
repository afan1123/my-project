/*
 * @Author: your name
 * @Date: 2020-09-24 19:25:37
 * @LastEditTime: 2020-09-29 13:41:15
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \图书管理系统\js\test.js
 */

/**
 * 功能一: 加载所有图书分类信息
 */

$(function () {
  var pageSize = 20;
  var pageIndex = 1;
  var URL_CATEGORY_LIST = "http://139.9.209.237/librarywebapi/category/list";
  var URL_CATEGORY_CREATE =
    "http://139.9.209.237/librarywebapi/category/create";
  var URL_CATEGORY_UPDATE =
    "http://139.9.209.237/librarywebapi/category/update";
  var categories = [];
  //加载全部数据
  $.get(URL_CATEGORY_LIST, function (res) {
    if (res.Code == 100) {
      categories = res.Data;
      loadCategoryData(categories);
      initSheets(categories);
    }
  });
  //查询
  $("#search_btn").on("click", function (e) {
    var txt = $("#search_txt").val();
    pageIndex = 1;
    var temp = categories;
    if (txt.trim()) {
      temp = temp.filter(function (item) {
        return item.Name.toLowerCase().indexOf(txt.toLowerCase()) >= 0;
      });
      loadCategoryData(temp);
      initSheets(temp);
    }
  });

  //保存新增
  $("#sure").on("click", function () {
    var obj = { name: $("#newName").val() };
    $.post(URL_CATEGORY_CREATE, obj).then(function (res) {
      if (res.Code == 100) {
        categories.unshift(res.Data);
        loadCategoryData(categories);
        $("#addBook .modal-footer span").text("").hide();
        $("#addBook").modal("hide");
      } else {
        $("#addBook .modal-footer span").show().text(res.Message);
      }
    });
  });
  //取消新增
  $("#cancelAdd").on("click", function () {
    $("#newName").val("");
    $("#newWeight").val("");
    $("#addBook .modal-footer span").text("").hide();
  });

  //取消编辑
  $("#cancelEdit").on("click", function () {
    $("#editor").removeProp("current_data");
    $("#editor").removeProp("current_row");
    $("#editor .modal-footer span").text("").hide();
  });
  //保存编辑
  $("#save").on("click", function () {
    var obj = {
      id: $("#editor").prop("current_data").Id,
      name: $("#name").val(),
    };
    $.post(URL_CATEGORY_CREATE, obj).then(function (res) {
      if (res.Code == 100) {
        $("#editor")
          .prop("current_row")
          .children("td:nth-child(2)")
          .text($("#name").val());
        $("#name").val("");
        $("#editor .modal-footer span").text("");
        $("#editor").removeProp("current_data");
        $("#editor").removeProp("current_row");
        $("#editor").modal("hide");
      } else {
        $(".modal-footer span").show().text(res.Message);
      }
    });
  });
  //加载行数据
  function loadCategoryData(datas) {
    $("tbody").html("");
    var startIndex = (pageIndex - 1) * pageSize;
    var endIndex = pageIndex * pageSize;
    var newArr = [];
    newArr = datas.slice(startIndex, endIndex);
    newArr.forEach(function (item, index) {
      var $row = createDataRow(item, index + startIndex);
      $row.appendTo($("tbody"));
    });
  }
  //创建行
  function createDataRow(data, index) {
    var $tr = $("<tr></tr>");
    $tr.html(
      `<td>` +
        (index + 1) +
        `</td> <td>` +
        data.Name +
        `</td><td>` +
        data.Priority +
        `</td>
        `
    );
    // 编辑事件
    $("<td></td>")
      .html(
        `<span title="编辑状态" data-toggle="modal" data-target="#editor" class="fa fa-edit"></span>`
      )
      .on("click", ".fa-edit", data, function () {
        $("#name").val(data.Name);
        $("#weight").val(data.Priority);
        $("#editor").prop("current_row", $tr).prop("current_data", data);
      })
      .appendTo($tr);
    return $tr;
  }
  //初始化页码
  function initSheets(arr) {
    $(".pagination").html("");
    $(".pagination").append(`<li class="disabled">
              <a href="#" aria-label="Previous"
                ><span aria-hidden="true">&laquo;</span></a
              >
            </li>`);
    var count = arr.length;
    for (var i = 0; i < Math.ceil(count / pageSize); i++) {
      createSheet(i, arr);
    }
    $(".pagination").append(`<li>
              <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>`);
    if ($(".pagination").children().length == 2) {
      return;
    }
    $(".pagination")
      .children("li:last")
      .on("click", function () {
        var next = $(".pagination").find(".active+li")[0];
        var current = $(".pagination").find(".active")[0];
        if (current.index + 1 == Math.ceil(count / pageSize)) {
          $(".pagination").children("li:last-child").addClass("disabled");
          return;
        }
        $(".pagination").children("li:last-child").removeClass("disabled");
        $(".pagination").find(".active").removeClass("active");
        next.className = "active";
        pageIndex = next.innerText;
        $(".row").html("");
        loadCategoryData(arr);
      });
    $(".pagination")
      .children("li:first")
      .on("click", function () {
        var pre = $(".pagination").find(".active").prev()[0];
        var current = $(".pagination").find(".active")[0];
        if (current.index == 0) {
          $(".pagination").children("li:first-child").addClass("disabled");
          return;
        }
        $(".pagination").children("li:first-child").removeClass("disabled");
        $(".pagination").find(".active").removeClass("active");
        pre.className = "active";
        pageIndex = pre.innerText;
        $(".row").html("");
        loadCategoryData(arr);
      });
    $(".pagination li:nth-child(2)").length == 1
      ? ($(".pagination li:nth-child(2)")[0].className = "active")
      : $(".pagination li:nth-child(2)")[0].className == "";
  }
  function createSheet(i, arr) {
    var $ul = $(".pagination");
    var a = createA(i, arr);
    $ul.append(a);
  }
  function createA(num, arr) {
    var a = document.createElement("li");
    a.index = num;
    a.innerHTML = `<span>` + (num + 1) + `<span class="sr-only"></span></span>`;
    a.onclick = function () {
      var pre = $(".pagination").find(".active")[0];
      if (this == pre) {
        return;
      }
      pre.className = "";
      this.className = "active";
      pageIndex = this.innerText;
      $(".row").html("");
      loadCategoryData(arr);
    };
    return a;
  }
});
