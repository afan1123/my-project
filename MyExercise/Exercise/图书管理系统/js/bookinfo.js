/*
 * @Author: your name
 * @Date: 2020-09-23 19:03:31
 * @LastEditTime: 2020-09-29 11:36:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \图书管理系统\js\bookinfo.js
 */
var flag = 0;
$(function () {
  var books = [];
  var books_Book = [];
  var pageSize = 18;
  var pageIndex = 1;
  var BOOK_LIST = 'http://139.9.209.237/librarywebapi/book/list';
  //加载全部数据
  $.get(BOOK_LIST, function (res) {
    books = res.Data;
    initBooks(res.Data);
    initSheets(res.Data);
  }).then(function () {
    books.forEach(function (item, index) {
      books_Book.push(item.Book);
    });
  });

  //加载分类列表
  $.get('http://139.9.209.237/librarywebapi/category/list', function (res) {
    res.Data.forEach(function (item, index) {
      $('#categroies').append(
        `<option  value="` + item.Id + `">` + item.Name + `</option>`
      );
    });
  });

  //加载出版社列表
  $.get('http://139.9.209.237/librarywebapi/publisher/list', function (res) {
    res.Data.forEach(function (item, index) {
      $('#publishers').append(
        `<option value="` + item.Id + `">` + item.Name + `</option>`
      );
    });
  });
  //查询
  $('#categroies').on('change', search);
  $('#publishers').on('change', search);
  $('#btn_search').on('click', search);
  function search() {
    $.get(
      BOOK_LIST +
        '?categoryId=' +
        $('#categroies').val() +
        '&publisherId=' +
        $('#publishers').val() +
        '&keyword=' +
        $('#txt_search').val(),
      function (res) {
        initBooks(res.Data);
        initSheets(res.Data);
      }
    );
  }
  //初始化页码
  function initSheets(arr) {
    $('.pagination').html('');
    $('.pagination').append(`<li class="disabled">
              <a href="#" aria-label="Previous"
                ><span aria-hidden="true">&laquo;</span></a
              >
            </li>`);
    var count = arr.length;
    for (var i = 0; i < Math.ceil(count / pageSize); i++) {
      createSheet(i, arr);
    }
    $('.pagination').append(`<li>
              <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>`);
    if ($('.pagination').children().length == 2) {
      return;
    }
    $('.pagination')
      .children('li:last')
      .on('click', function () {
        var next = $('.pagination').find('.active+li')[0];
        var current = $('.pagination').find('.active')[0];
        if (current.index + 1 == Math.ceil(count / pageSize)) {
          $('.pagination').children('li:last-child').addClass('disabled');
          return;
        }
        $('.pagination').children('li:last-child').removeClass('disabled');
        $('.pagination').find('.active').removeClass('active');
        next.className = 'active';
        pageIndex = next.innerText;
        $('#box-body').html('');
        initBooks(arr);
      });
    $('.pagination')
      .children('li:first')
      .on('click', function () {
        var pre = $('.pagination').find('.active').prev()[0];
        var current = $('.pagination').find('.active')[0];
        if (current.index == 0) {
          $('.pagination').children('li:first-child').addClass('disabled');
          return;
        }
        $('.pagination').children('li:first-child').removeClass('disabled');
        $('.pagination').find('.active').removeClass('active');
        pre.className = 'active';
        pageIndex = pre.innerText;
        $('#box-body').html('');
        initBooks(arr);
      });
    $('.pagination li:nth-child(2)').length == 1
      ? ($('.pagination li:nth-child(2)')[0].className = 'active')
      : $('.pagination li:nth-child(2)')[0].className == '';
  }
  function createSheet(i, arr) {
    var $ul = $('.pagination');
    var a = createA(i, arr);
    $ul.append(a);
  }
  function createA(num, arr) {
    var a = document.createElement('li');
    a.index = num;
    a.innerHTML =
      `<span>` + (num + 1) + ` <span class="sr-only"></span></span>`;
    a.onclick = function () {
      var pre = $('.pagination').find('.active')[0];
      if (this == pre) {
        return;
      }
      pre.className = '';
      this.className = 'active';
      pageIndex = this.innerText;
      $('#box-body').html('');
      initBooks(arr);
    };
    return a;
  }

  //初始化图书列表
  function initBooks(arr) {
    $('#box-body').html('');
    var startIndex = (pageIndex - 1) * pageSize;
    var endIndex = pageIndex * pageSize;
    var newArr = [];
    newArr = arr.slice(startIndex, endIndex);
    newArr.forEach(function (item, index) {
      var $div = createDiv(item);
      $div.appendTo($('#box-body'));
    });
  }

  function createDiv(item) {
    var $content = $(
      ` <div class="divBox col-lg-2 col-md-3 col-sm-6 col-xm-1">
                <div>
                  <div class="wrapImg">
                    <img
                      src="` +
        item.Book.Image +
        `"
                      alt=""
                    />
                  </div>
                  <div class="title">` +
        item.Book.Name +
        `</div>
                  <div class="wrapWords">
                  <span class="publish col-lg-6 col-md-7 col-sm-8 col-xm-9">` +
        item.Book.Publisher.Name +
        `</span>
                  <div class="kinds col-lg-6 col-md-5 col-sm-4 col-xm-3">` +
        item.Book.Category.Name +
        `</div>
                  </div>
                  <span class="putawayCount">` +
        item.Number +
        `/` +
        item.Number +
        `</span>
        <div class="footer">
          <span class="fa fa-edit"></span>
            <span class="fa fa-cloud-upload"></span>
                </div>
                </div>
              </div>`
    )
      .attr('div-id', item.Book.Id)
      .on('click', '.fa-edit', item, function () {
        flag = 2;
        showEdit();
        $('#ISBN').val(item.Book.ISBN);
        $('#img_wrap img').prop('src', item.Book.Image);
        $('#bookname').val(item.Book.Name);
        $('#pubData').val(item.Book.PublishDate);
        $('#category')
          .find("option[value='" + item.Book.Category.Id + "']")
          .attr('selected', true);
        $('#publisher')
          .find('option[value=' + item.Book.Publisher.Id + ']')
          .attr('selected', true);
        $('#editor')
          .find('option[value=' + item.Book.Author.Id + ']')
          .attr('selected', true);
        $('.box').prop('current-data', item.Book.Id);
      })
      .on('click', '.fa-cloud-upload', item, function () {
        $('#putaway').modal('show');
        $('#putaway img').prop('src', item.Book.Image);
        $('#putawayTitle').text(item.Book.Name);
        $('#putawayAuthor').text(item.Book.Author.Name);
        $('#putawayCategory').text(item.Book.Category.Name);
        $('#putawayDate').text(item.Book.PublishDate);
        $('#putaway').prop('current-bookId', item.Book.Id);
      });
    return $content;
  }
});
var BOOK_PUTAWAY_UPDATE = 'http://139.9.209.237/librarywebapi/book/putaway';
$('#putawaySave').on('click', function () {
  var obj = {
    bookId: $('#putaway').prop('current-bookId'),
    count: $('#putawayNumber').val(),
  };
  $.post(BOOK_PUTAWAY_UPDATE, obj).then(function (res) {
    if (res.Code == 100) {
      var div_Id = $('#putaway').prop('current-bookId');
      var $spanCount = $('.divBox[div-id=' + div_Id + '] .putawayCount');
      $('#putaway').modal('hide');
      $spanCount.html(res.Data.Number + '/' + res.Data.Number);
    }
  });
});
function showEdit() {
  $('.box').show();
  $('.box-header').hide();
  $('#box-body').hide();
  $('.pagination').hide();
}
function hideEdit() {
  $('.box').hide();
  $('.box-header').show();
  $('#box-body').show();
  $('.pagination').show();
  flag = 0;
}
$(function () {
  var book_Categroy_List = 'http://139.9.209.237/librarywebapi/category/list';
  var book_Publisher_List = 'http://139.9.209.237/librarywebapi/publisher/list';
  var book_Editor_List = 'http://139.9.209.237/librarywebapi/author/list';
  $('.box-title').on('click', function () {
    flag = 1;
    showEdit();
  });
  $('#cancelEdit').on('click', hideEdit);

  getData(book_Categroy_List, '#category');
  getData(book_Publisher_List, '#publisher');
  getData(book_Editor_List, '#editor');

  function getData(url, id) {
    $.get(url, function (res) {
      if (res.Code == 100) {
        loadOpts(res.Data, id);
      }
    });
  }

  function loadOpts(datas, selector) {
    datas.forEach(function (item, index) {
      var $opt = createOpt(item);
      $opt.appendTo(selector);
    });
  }
  function createOpt(data) {
    $opt = $('<option></option>');
    $opt.prop('value', data.Id);
    $opt.text(data.Name);
    return $opt;
  }
});
$(function () {
  $('.btnFile').on('click', function () {
    $('.btnFile input')[0].click();
  });
  $('.btnFile input').on('change', function (e) {
    var file = e.target.files[0];
    console.log(file);
    var windowURL = window.URL || window.webkitURL || window.mozURL;
    var imgSrc = windowURL.createObjectURL(file);
    $('#img_wrap img').prop('src', imgSrc);
  });
});
$(function () {
  var book_URL_Create = 'http://139.9.209.237/librarywebapi/book/create';
  var BOOK_URL_UPDATE = 'http://139.9.209.237/librarywebapi/book/update';
  $('#saveEdit').on('click', function () {
    var obj = {
      isbn: $('#ISBN').val(),
      name: $('#bookname').val(),
      publishDate: $('#pubData').val(),
      categoryId: $('#category').prop('value'),
      publisherId: $('#publisher').prop('value'),
      authorId: $('#editor').prop('value'),
      introduce: $('textarea').val(),
      image: $('#img_wrap input')[0].files[0],
    };
    if (flag == 1) {
      editBook(obj, book_URL_Create);
      return;
    }
    if (flag == 2) {
      obj.id = $('.box').prop('current-data');
      editBook(obj, BOOK_URL_UPDATE);
    }
  });
});

function editBook(obj, url) {
  var fd = new FormData();
  for (var key in obj) {
    fd.append(key, obj[key]);
  }
  $.ajax({
    type: 'post',
    url: url,
    contentType: false,
    processData: false,
    data: fd,
    success: function (res) {
      location.reload();
    },
  });
}
