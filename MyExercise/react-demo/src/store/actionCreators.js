import {
  CHANGE_INPUT,
  ORDER_CHANGE,
  ADD_ITEM,
  DELETE_ITEM,
  GET_LIST,
  INSUERD_CHANGE,
  SUBMIT_LIST,
  GET_CATEGORY,
  GET_CATEGORY_LIST,
  DATA_CHANGE,
  RESET_LIST,
} from "./actionTypes";
import category from "../service/category";

export const changeInputAction = (value) => ({
  type: CHANGE_INPUT,
  value,
});

export const addItemAction = () => ({
  type: ADD_ITEM,
});

export const deleteItemAction = (index) => ({
  type: DELETE_ITEM,
  index,
});

export const getListAction = (data) => ({
  type: GET_LIST,
  data,
});

export const submitListAction = (info) => ({
  type: SUBMIT_LIST,
  info,
});

export const resetListAction = () => ({
  type: RESET_LIST,
});

export const orderChangeAction = (value) => ({
  type: ORDER_CHANGE,
  value,
});

export const insuredIdentifyChangeAction = (value) => ({
  type: INSUERD_CHANGE,
  value,
});

export const getCategoryListAction = (data) => ({
  type: GET_CATEGORY_LIST,
  data,
});

export const dataChangeAction = (data) => ({
  type: DATA_CHANGE,
  data,
});

export const getCategoryAction = () => {
  return async (dispatch) => {
    try {
      let res = await category.getList();
      const action = getCategoryListAction(res.Data);
      dispatch(action);
    } catch (error) {
      console.log(error);
    }
  };
};
