import {createStore, applyMiddleware, compose} from "redux";
// 引入配置的reudcer
import reducer from "./reducer";
//引入saga
import createSagaMiddleware from "redux-saga";
// 引入配置的saga文件
import mySagas from "./sagas";
import thunk from "redux-thunk";

const sagaMiddleware = createSagaMiddleware(); //创建saga中间件
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
  : compose;
const enhancer = composeEnhancers(applyMiddleware(thunk));
const store = createStore(reducer, enhancer); // 创建数据存储仓库

// sagaMiddleware.run(mySagas);

export default store;
