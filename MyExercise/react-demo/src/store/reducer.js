import {
  CHANGE_INPUT,
  ORDER_CHANGE,
  ADD_ITEM,
  DELETE_ITEM,
  GET_LIST,
  INSUERD_CHANGE,
  SUBMIT_LIST,
  GET_CATEGORY_LIST,
  RESET_LIST,
  DATA_CHANGE,
} from "./actionTypes";

const defaultInfo = {
  order: "",
  product: "0",
  insured: "",
  purchase: "0",
  applicant: "",
  branch: "",
  medium: "",
  date: "",
};
const defaultState = {
  inputValue: "Write Sth",
  list: [],
  info: defaultInfo,
  data: [],
};
export default (state = defaultState, action) => {
  if (action.type === CHANGE_INPUT) {
    let newState = JSON.parse(JSON.stringify(state)); //深度拷贝state
    newState.inputValue = action.value;
    return newState;
  }
  if (action.type === ORDER_CHANGE) {
    let newState = JSON.parse(JSON.stringify(state));
    newState.info.order = action.value;
    return newState;
  }
  if (action.type === SUBMIT_LIST) {
    let newState = JSON.parse(JSON.stringify(state));
    // newState.info = action.info;
    console.log(newState);
    return newState;
  }
  if (action.type === RESET_LIST) {
    let newState = JSON.parse(JSON.stringify(state));
    newState.info = defaultInfo;
    console.log(newState.info);
    return newState;
  }
  if (action.type === DATA_CHANGE) {
    let newState = JSON.parse(JSON.stringify(state));
    let type = action.data.itemType;
    newState.info[type] = action.data.value;
    console.log(newState);
    return newState;
  }
  if (action.type === INSUERD_CHANGE) {
    let newState = JSON.parse(JSON.stringify(state));
    newState.info.insured = action.value;
    return newState;
  }
  //state值只能传递，不能使用
  if (action.type === ADD_ITEM) {
    //根据type值，编写业务逻辑
    let newState = JSON.parse(JSON.stringify(state));
    newState.list.push(newState.inputValue); //push新的内容到列表中去
    newState.inputValue = "";
    return newState;
  }
  if (action.type === DELETE_ITEM) {
    let newState = JSON.parse(JSON.stringify(state));
    newState.list.splice(action.index, 1);
    return newState;
  }
  if (action.type === GET_LIST) {
    let newState = JSON.parse(JSON.stringify(state));
    newState.data = action.data;
    return newState;
  }
  if (action.type === GET_CATEGORY_LIST) {
    let newState = JSON.parse(JSON.stringify(state));
    newState.data = action.data;
    console.log(action.data);
    return newState;
  }

  return state;
};
