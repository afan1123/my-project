export const CHANGE_INPUT = "changeInput";
export const ADD_ITEM = "addItem";
export const DELETE_ITEM = "deleteItem";
export const GET_LIST = "getList";
export const ORDER_CHANGE = "orderChange";
export const INSUERD_CHANGE = "insuredChange";
export const SUBMIT_LIST = "submitList";
export const RESET_LIST = "resetList";
export const GET_CATEGORY = "getCategory";
export const GET_CATEGORY_LIST = "getCategoryList";
export const DATA_CHANGE = "dataChange";
