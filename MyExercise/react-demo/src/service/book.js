import http from "./http";
function getList(categoryId = null, publisherId = null, keyword = null) {
  return http.get("book/list", {params: {categoryId, publisherId, keyword}});
}

export default {
  getList,
};
