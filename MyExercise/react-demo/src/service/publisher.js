import http from "./http";
function getList() {
  return http.get("publisher/list");
}

export default {
  getList,
};
