import http from "./http";
function getList() {
  return http.get("category/list");
}

export default {
  getList,
};
