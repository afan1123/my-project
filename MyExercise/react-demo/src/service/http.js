import axios from "axios";

axios.defaults.baseURL = "/api/";
// axios.proxy = {
//   host: "127.0.0.1",
//   port: 3000,
//   auth: {
//     username: "mikeymike",
//     password: "rapunz3l",
//   },
// };

// 添加请求拦截器
axios.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  function (res) {
    // 对响应数据做点什么
    return res.data;
  },
  function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);

const http = {
  get: axios.get,
  post: axios.post,
};
export default http;
