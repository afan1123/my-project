import React from "react";
import ReactDOM from "react-dom";
import "./commonCss/common.css";

import App from "./App";

ReactDOM.render(<App />, document.getElementById("root"));
