import React, {Component} from "react";
import {Provider} from "react-redux";
import store from "./store";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";

import "antd/dist/antd.css";
import LoginPage from "pages/login";
import MainPage from "pages/main";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  // 路由拦截功能，若没有登录，则一直停留在登录页
  toLogin = () => {
    let localUser = JSON.parse(localStorage.getItem("user"));
    if (localUser) {
      return;
    }
    return <Redirect to="/login"></Redirect>;
  };
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/login" component={LoginPage} exact></Route>
            <Route path="/" component={MainPage}></Route>
          </Switch>
          {this.toLogin()}
        </Router>
      </Provider>
    );
  }
}

export default App;
