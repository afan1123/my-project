import React, {Component, Fragment} from "react";
import {FormDate, FormInput, FormSelect, FormSpace, FormRange} from "./indexUi";
import {Row} from "antd";
class FormCell extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  FormCellType(data, index) {
    const {labelVal, type, required} = data;
    if (type === "date") {
      return (
        <FormDate
          required={required}
          labelVal={labelVal}
          key={index + "--" + type}
        ></FormDate>
      );
    }
    if (type === "input") {
      return (
        <FormInput
          required={required}
          labelVal={labelVal}
          key={index + "--" + type}
        ></FormInput>
      );
    }
    if (type === "select") {
      return (
        <FormSelect
          required={required}
          labelVal={labelVal}
          key={index + "--" + type}
        ></FormSelect>
      );
    }
    if (type === "range") {
      return (
        <FormRange
          required={required}
          labelVal={labelVal}
          key={index + "--" + type}
        ></FormRange>
      );
    }
    return <FormSpace key={index + "--" + "space"}></FormSpace>;
  }
  render() {
    const {formData} = this.props;
    return (
      <div className="formWrap">
        <Row>
          {formData.map((item, index) => {
            return this.FormCellType(item, index);
          })}
        </Row>
      </div>
    );
  }
}

export default FormCell;
