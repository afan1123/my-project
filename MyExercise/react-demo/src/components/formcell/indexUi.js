import React, {Component} from "react";
import "./index.less";
import {Col, Select, DatePicker, Space} from "antd";
const {RangePicker} = DatePicker;
const {Option} = Select;
function FormInput(props) {
  const {labelVal, required} = props;
  return (
    <Col xs={8} sm={8} md={8} lg={8} xl={8}>
      <div className="formInput">
        <div className="labelWrap">
          <label htmlFor="order">
            {labelVal}
            {required ? <span style={{color: "red"}}>*</span> : ""}
          </label>
        </div>
        <div className="inputWrap">
          <input
            defaultValue={""}
            // onChange={(e) => {
            //   dataChange("order", e);
            // }}
            type="text"
            id="order"
            placeholder="请输入..."
          />
        </div>
      </div>
    </Col>
  );
}

function FormSelect(props) {
  const {labelVal, required} = props;
  return (
    <Col xs={8} sm={8} md={8} lg={8} xl={8}>
      <div className="formInput">
        <div className="labelWrap">
          <label htmlFor="order">
            {labelVal}
            {required ? <span style={{color: "red"}}>*</span> : ""}
          </label>
        </div>
        <div className="inputWrap">
          <Select
            defaultValue="jack"
            style={{width: 120}}
            // onChange={handleChange}
          >
            <Option value="jack">Jack</Option>
          </Select>
        </div>
      </div>
    </Col>
  );
}

function FormDate(props) {
  const {labelVal, required} = props;
  return (
    <Col xs={8} sm={8} md={8} lg={8} xl={8}>
      <div className="formInput">
        <div className="labelWrap">
          <label htmlFor="order">
            {labelVal}
            {required ? <span style={{color: "red"}}>*</span> : ""}
          </label>
        </div>
        <div className="inputWrap">
          <DatePicker />
        </div>
      </div>
    </Col>
  );
}

function FormRange(props) {
  const {labelVal, required} = props;
  return (
    <Col xs={8} sm={8} md={8} lg={8} xl={8}>
      <div className="formInput">
        <div className="labelWrap">
          <label htmlFor="order">
            {labelVal}
            {required ? <span style={{color: "red"}}>*</span> : ""}
          </label>
        </div>
        <div className="inputWrap">
          <RangePicker placeholder={["开始时间", "结束时间"]} />
        </div>
      </div>
    </Col>
  );
}

function FormSpace() {
  return <Col xs={8} sm={8} md={8} lg={8} xl={8}></Col>;
}

export {FormDate, FormRange, FormInput, FormSelect, FormSpace};
