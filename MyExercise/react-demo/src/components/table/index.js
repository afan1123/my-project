import React from "react";
import {Table, Space, Popconfirm, Button} from "antd";

export function Newtable(props) {
  let {data, confirm} = props;

  const columns = [
    {
      title: "编号",
      dataIndex: "index",
      key: "index",
    },
    {
      title: "分类名",
      dataIndex: "Name",
      key: "Name",
    },
    {
      title: "权重",
      dataIndex: "Priority",
      key: "Priority",
    },
    {
      title: "操作",
      key: "action",
      render: () => (
        <Space size="middle">
          <Popconfirm
            placement="topRight"
            title="你确定要删除吗?"
            onConfirm={() => {
              confirm();
            }}
            okText="确定"
            cancelText="取消"
          >
            <Button>删除</Button>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  let list = data;
  list.forEach((item, index) => {
    item.key = item.Id;
    item.index = index + 1;
  });
  return (
    <Table
      columns={columns}
      dataSource={list}
      pagination={{defaultPageSize: 6}}
    />
  );
}
