import React, {Component, Fragment} from "react";
import {Form, Input, Button, Tabs, message} from "antd";
import {Iconfont} from "assets/iconfonts";
import "./index.less";
const {TabPane} = Tabs;

function callback(key) {
  console.log(key);
}

class Demo extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  onFinish = (values) => {
    const {username, password} = {...values};
    const user = {username, password};
    localStorage.setItem("user", JSON.stringify(user));
    message.success("恭喜您！登陆成功！").then(() => {
      this.props.backFn();
    });
  };

  onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  componentDidUpdate() {
    const {currentUser} = this.props;
    if (currentUser === null) {
      this.formRef.setFieldsValue({
        username: "",
        password: "",
      });
      return;
    }
    this.formRef.setFieldsValue({
      username: currentUser.username,
      password: currentUser.password,
    });
  }
  render = () => {
    const layout = {
      wrapperCol: {
        span: 24,
      },
    };
    const tailLayout = {
      wrapperCol: {
        span: 24,
      },
    };
    return (
      <div className="form">
        <div className="formTitle">
          <h1>
            <Iconfont type="icon-logo"></Iconfont>
          </h1>
        </div>
        <Tabs defaultActiveKey="1" onChange={callback}>
          <TabPane tab="渠道业务员登录" key="1">
            <Form
              {...layout}
              name="basic"
              initialValues={{
                remember: true,
              }}
              onFinish={this.onFinish}
              onFinishFailed={this.onFinishFailed}
              ref={(ref) => {
                this.formRef = ref;
              }}
            >
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: "请输入你的用户名！",
                  },
                ]}
              >
                <Input
                  ref={(el) => (this.username = el)}
                  placeholder="登录账号..."
                />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "请输入你的密码",
                  },
                ]}
              >
                <Input.Password
                  ref={(el) => (this.password = el)}
                  placeholder="登录密码..."
                  value="1"
                />
              </Form.Item>

              <Form.Item {...tailLayout}>
                <div className="verify">
                  <span className="outRound">
                    <span className="inRound"></span>
                  </span>
                  <span>点击进行验证</span>
                </div>
              </Form.Item>
              <Form.Item {...tailLayout}>
                <Button
                  type="primary"
                  style={{width: "100%", padding: "5px 0"}}
                  htmlType="submit"
                >
                  提交
                </Button>
              </Form.Item>
              <Form.Item {...tailLayout}>
                <div className="resetPassword">
                  <span>重置密码</span>
                </div>
              </Form.Item>
            </Form>
          </TabPane>
          <TabPane tab="渠道管理员登录" key="2">
            <Form
              {...layout}
              name="basic"
              initialValues={{
                remember: true,
              }}
              onFinish={this.onFinish}
              onFinishFailed={this.onFinishFailed}
            >
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: "请输入你的用户名！",
                  },
                ]}
              >
                <Input
                  ref={(el) => (this.username = el)}
                  placeholder="登录账号..."
                />
              </Form.Item>

              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "请输入你的密码",
                  },
                ]}
              >
                <Input.Password
                  ref={(el) => (this.password = el)}
                  placeholder="登录密码..."
                />
              </Form.Item>

              <Form.Item {...tailLayout}>
                <div className="verify">
                  <span className="outRound">
                    <span className="inRound"></span>
                  </span>
                  <span>点击进行验证</span>
                </div>
              </Form.Item>
              <Form.Item {...tailLayout}>
                <Button
                  type="primary"
                  style={{width: "100%", padding: "5px 0"}}
                  htmlType="submit"
                >
                  提交
                </Button>
              </Form.Item>
              <Form.Item {...tailLayout}>
                <div className="resetPassword">
                  <span>重置密码</span>
                </div>
              </Form.Item>
            </Form>
          </TabPane>
        </Tabs>
      </div>
    );
  };
}

export default Demo;
