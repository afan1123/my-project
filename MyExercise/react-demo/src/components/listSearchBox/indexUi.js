import React from "react";
import "./index.css";
import {Row, Col, Button, Select} from "antd";
const {Option} = Select;

export function SearchBoxUi(props) {
  let {reset, info, submit, purchaseChange, productChange, dataChange} = props;
  return (
    <div>
      <Row>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className={"inputCell"}>
            <label htmlFor="order">保单号：</label>
            <input
              value={info.order}
              onChange={(e) => {
                dataChange("order", e);
              }}
              type="text"
              id="order"
              placeholder="请输入..."
            />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className={"inputCell"}>
            <label htmlFor="insuredIdentify">被保人证件号：</label>
            <input
              type="text"
              value={info.insured}
              onChange={(e) => {
                dataChange("insured", e);
              }}
              id="insuredIdentify"
              placeholder="请输入..."
            />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className={"inputCell"}>
            <label htmlFor="productName">产品名称：</label>
            <Select
              value={info.product}
              onChange={(value) => {
                dataChange("product", value);
              }}
              defaultValue={"0"}
              style={{width: 120}}
            >
              <Option value="0">请选择</Option>
              <Option value="1">请选择1</Option>
              <Option value="2">请选择2</Option>
            </Select>
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className={"inputCell"}>
            <label htmlFor="purchaseType">缴费类型：</label>
            <Select
              defaultValue="0"
              value={info.purchase}
              style={{width: 120}}
              onChange={(value) => {
                dataChange("purchase", value);
              }}
            >
              <Option value="0">请选择</Option>
              <Option value="1">请选择1</Option>
              <Option value="2">请选择2</Option>
            </Select>
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className={"inputCell"}>
            <label htmlFor="applicantIdentify">投保人证件号：</label>
            <input
              value={info.applicant}
              onChange={(e) => {
                dataChange("applicant", e);
              }}
              type="text"
              id="applicantIdentify"
              placeholder="请输入..."
            />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className={"inputCell"}>
            <label htmlFor="branch">分支机构简称：</label>
            <input
              value={info.branch}
              onChange={(e) => {
                dataChange("branch", e);
              }}
              type="text"
              id="branch"
              placeholder="请输入..."
            />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className={"inputCell"}>
            <label htmlFor="mediumName">中介业务员姓名：</label>
            <input
              value={info.medium}
              onChange={(e) => {
                dataChange("medium", e);
              }}
              type="text"
              id="mediumName"
              placeholder="请输入..."
            />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className={"inputCell"}>
            <label htmlFor="date">投保日期：</label>
            <input
              value={info.date}
              onChange={(e) => {
                dataChange("date", e);
              }}
              type="text"
              id="date"
              placeholder="请输入..."
            />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div style={{padding: "0 10px"}}>
            <Button
              type="primary"
              style={{
                border: 0,
                backgroundColor: "#00BC70",
                marginRight: "10px",
              }}
              onClick={() => {
                let info = {};
                submit(info);
              }}
            >
              查询
            </Button>
            <Button type="default" onClick={reset}>
              重置
            </Button>
          </div>
        </Col>
      </Row>
    </div>
  );
}
