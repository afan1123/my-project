import React, {Component} from "react";
import {connect} from "react-redux";
import {SearchBoxUi} from "./indexUi";
import {
  submitListAction,
  resetListAction,
  dataChangeAction,
} from "store/actionCreators";

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  purchaseChange(value) {
    console.log(value);
  }
  productChange(value) {
    console.log(value);
  }
  dataChange(type, item) {
    if (type === "order") {
      console.log("order:" + item);
    }
  }
  render() {
    const {submit, reset, info, dataChange} = this.props;
    return (
      <SearchBoxUi
        purchaseChange={this.purchaseChange}
        productChange={this.productChange}
        submit={submit}
        info={info}
        reset={reset}
        dataChange={dataChange}
      ></SearchBoxUi>
    );
  }
}

const stateToProps = (state) => {
  return {
    info: state.info,
  };
};

const dispatchToProps = (dispatch) => {
  return {
    dataChange(type, e) {
      let action = {};
      if (type === "product" || type === "purchase") {
        action = dataChangeAction({
          itemType: type,
          value: e,
        });
      } else {
        action = dataChangeAction({
          itemType: type,
          value: e.target.value,
        });
      }

      dispatch(action);
    },
    submit(info) {
      let action = submitListAction(info);
      dispatch(action);
    },
    reset() {
      let action = resetListAction();
      dispatch(action);
    },
  };
};

export default connect(stateToProps, dispatchToProps)(SearchBox);
