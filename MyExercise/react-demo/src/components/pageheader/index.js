import React, {Component, Fragment} from "react";
import {PageHeader} from "antd";
import "./index.css";

class NewPageheader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Fragment>
        <PageHeader className={"sitePageHeader"} title={this.props.history} />
      </Fragment>
    );
  }
}

export default NewPageheader;
