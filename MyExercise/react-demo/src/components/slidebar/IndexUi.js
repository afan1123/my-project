import React, {Component, Fragment} from "react";
import {Menu, Popconfirm, message} from "antd";
import {
  AppstoreOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  PieChartOutlined,
  DesktopOutlined,
  ContainerOutlined,
  MailOutlined,
} from "@ant-design/icons";
const {SubMenu} = Menu;
const active = {
  "/welcome": "1",
  "/home": "2",
  "/list": "3",
  "/cart": "4",
  "/mine": "5",
};
function Newmenu(props) {
  return (
    <Fragment>
      <Menu
        className={"antMenu"}
        defaultSelectedKeys={[active[props.title]]}
        defaultOpenKeys={["sub1", "sub2", "sub3", "sub4"]}
        mode="inline"
        theme="dark"
        inlineCollapsed={props.collapsed}
        style={{overflowY: "auto", height: "100%"}}
      >
        <Menu.Item
          key="1"
          onClick={() => {
            props.toPage("/welcome");
          }}
          icon={<PieChartOutlined />}
        >
          欢迎页
        </Menu.Item>
        <SubMenu key="sub1" icon={<MailOutlined />} title="首页">
          <Menu.Item
            key="2"
            onClick={() => {
              props.toPage("/home");
            }}
            icon={<PieChartOutlined />}
          >
            首页
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub2" icon={<MailOutlined />} title="列表业">
          <Menu.Item
            key="3"
            onClick={() => {
              props.toPage("/list");
            }}
            icon={<PieChartOutlined />}
          >
            列表页
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub3" icon={<MailOutlined />} title="购物车">
          <Menu.Item
            key="4"
            onClick={() => {
              props.toPage("/cart");
            }}
            icon={<PieChartOutlined />}
          >
            购物车
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub4" icon={<MailOutlined />} title="我的">
          <Menu.Item
            key="5"
            onClick={() => {
              props.toPage("/mine");
            }}
            icon={<PieChartOutlined />}
          >
            我的
          </Menu.Item>
        </SubMenu>
        <Menu.Item key="20" icon={<PieChartOutlined />}>
          <Popconfirm
            placement="topRight"
            title={"您确定要注销吗？"}
            onConfirm={props.confirm}
            okText="确定"
            cancelText="取消"
          >
            <span>注销</span>
          </Popconfirm>
        </Menu.Item>
      </Menu>
    </Fragment>
  );
}
export default Newmenu;
