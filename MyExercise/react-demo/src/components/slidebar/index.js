import React, {Component, Fragment} from "react";
import {message, Spin} from "antd";
import Newmeun from "./IndexUi";
import "./index.css";

class SlideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
    };
  }
  confirm = () => {
    message.info("注销成功.即将返回登录页...").then(() => {
      localStorage.setItem("user", null);
      this.toPage("/login");
    });
  };
  toPage(page) {
    this.props.toPage(page);
  }
  render() {
    return (
      <Newmeun
        title={this.props.title}
        confirm={this.confirm}
        collapsed={this.state.false}
        toPage={(page) => {
          this.toPage(page);
        }}
      ></Newmeun>
    );
  }
}

export default SlideBar;
