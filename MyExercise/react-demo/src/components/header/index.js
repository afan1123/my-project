import React from "react";
import {BulbOutlined, UserOutlined} from "@ant-design/icons";
import logo from "assets/images/logo@2x.png";
import "./index.css";
export function Header() {
  return (
    <h1 className={"header"}>
      <div className="imgWrap">
        <img src={logo} alt="" />
      </div>
      <div className="rightWrap">
        <BulbOutlined
          style={{
            marginRight: "10px",
            fontSize: "20px",
            color: "rgba(255,255,255,.7)",
          }}
        />
        <BulbOutlined
          style={{
            marginRight: "10px",
            fontSize: "20px",
            color: "rgba(255,255,255,.7)",
          }}
        />
        <BulbOutlined
          style={{
            marginRight: "10px",
            fontSize: "20px",
            color: "rgba(255,255,255,.7)",
          }}
        />
        <UserOutlined style={{margin: "10px", fontSize: "20px"}}></UserOutlined>
      </div>
    </h1>
  );
}
