import React, {Component, Fragment} from "react";
import {Card} from "antd";
import {EditOutlined} from "@ant-design/icons";
import styled from "styled-components";

const Refragment = styled.div`
  .ant-card-cover {
    border-right: 1px solid #f0f0f0;
    border-left: 1px solid #f0f0f0;
  }
`;
const {Meta} = Card;
class BookCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const book = this.state.data ? this.state.data.Book : null;
    return (
      <Refragment>
        <Card
          hoverable
          style={{
            width: "80%",
            margin: "auto",
            paddingTop: "10px",
            marginBottom: "10px",
          }}
          cover={
            <img
              alt=""
              style={{minHeight: "275px", width: "90%", margin: "auto"}}
              src={
                this.props.dataSource
                  ? this.props.dataSource.Image
                  : "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
              }
            />
          }
          actions={[<EditOutlined key="edit" />]}
        >
          <Meta
            title={this.props.dataSource ? this.props.dataSource.Name : ""}
          />
        </Card>
      </Refragment>
    );
  }
}

export default BookCard;
