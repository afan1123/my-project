import React, {Component} from "react";
import {Modal} from "antd";
import styled from "styled-components";

const Info = styled.div`
  color: red;
  text-align: center;
  img {
    display: inline-block;
    margin: auto;
    width: 50%;
  }
`;

class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {visible: false, detail: null};
  }

  showModal = (detail) => {
    this.setState(
      {
        detail,
        visible: true,
      },
      () => {
        console.log(this.state.detail);
      }
    );
  };

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      detail: null,
    });
  };

  render() {
    const {Name, Image} = {...this.state.detail};
    return (
      <Modal
        title={Name}
        visible={this.state.visible}
        footer={null}
        onCancel={this.handleCancel}
      >
        {/* <p>
          <img src={Image}></img>
        </p> */}
        <Info>
          <div>
            <img src={Image} />
          </div>
        </Info>
      </Modal>
    );
  }
}

export default Detail;
