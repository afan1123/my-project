import React, {Component, lazy, Suspense} from "react";
import {Select, Row, Col, Input, Pagination} from "antd";
import "./index.css";
import book from "service/book";
import category from "service/category";
import publisher from "service/publisher";
import Detail from "./detail";
import {BookCard} from "components";
import {Spin} from "antd";
const {Option} = Select;
const {Search} = Input;

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      books: [],
      categories: [],
      publishers: [],
      result: [],
      pageSize: 6,
      pageIndex: 1,
      categoryId: 0,
      publisherId: 0,
      keyword: "",
    };
  }
  pageChange = (pageIndex) => {
    this.setState({pageIndex}, () => {
      this.searchResult();
    });
  };

  searchBooks = () => {
    this.getBookList().then((res) => {
      this.setState({books: res.Data}, () => {
        this.searchResult();
      });
    });
  };

  searchResult = () => {
    let startIndex = (this.state.pageIndex - 1) * this.state.pageSize;
    let endIndex = this.state.pageIndex * this.state.pageSize;
    let books = this.state.books;
    this.setState({result: books.slice(startIndex, endIndex)});
  };

  onSearch = (value) => {
    this.setState({keyword: value}, () => {
      this.searchBooks();
    });
  };

  cateChange = (value) => {
    this.setState({categoryId: value}, () => {
      this.state.pageIndex = 1;
      this.searchBooks();
    });
  };

  pubChange = (value) => {
    this.setState({publisherId: value}, () => {
      this.state.pageIndex = 1;
      this.searchBooks();
    });
  };

  async getBookList() {
    try {
      let {categoryId, publisherId, keyword} = {...this.state};
      if (categoryId == 0) {
        categoryId = null;
      }
      if (publisherId == 0) {
        publisherId = null;
      }
      if (keyword == "") {
        keyword = null;
      }
      return await book.getList(categoryId, publisherId, keyword);
    } catch (error) {
      console.log(error);
    }
  }

  async getCategories() {
    try {
      return await category.getList();
    } catch (error) {
      console.log(error);
    }
  }

  async getPublishers() {
    try {
      return await publisher.getList();
    } catch (error) {
      console.log(error);
    }
  }

  showModel(detail) {
    this.model.showModal(detail.Book);
  }

  componentDidMount() {
    this.getBookList().then((res) => {
      this.setState({books: res.Data}, () => {
        this.searchResult();
      });
    });
    this.getCategories().then((res) => {
      this.setState({categories: res.Data});
    });
    this.getPublishers().then((res) => {
      this.setState({publishers: res.Data});
    });
  }
  render() {
    const {categories, publishers, books, result} = {...this.state};
    return (
      <div>
        <div className="right-wrap">
          <div className="searchBox">
            <Row>
              <Col span={6}>
                <Select
                  defaultValue="0"
                  style={{width: 120}}
                  onChange={this.cateChange}
                >
                  <Option value="0">全部分类</Option>
                  {categories.map((item, index) => {
                    return (
                      <Option key={item.Id} value={item.Id}>
                        {item.Name}
                      </Option>
                    );
                  })}
                </Select>
              </Col>
              <Col span={6}>
                <Select
                  defaultValue="0"
                  style={{width: 120}}
                  onChange={this.pubChange}
                >
                  <Option value="0">全部出版社</Option>
                  {publishers.map((item, index) => {
                    return (
                      <Option key={item.Id} value={item.Id}>
                        {item.Name}
                      </Option>
                    );
                  })}
                </Select>
              </Col>
              <Col span={12}>
                <Search
                  placeholder="请输入书名..."
                  onSearch={this.onSearch}
                  enterButton
                />
              </Col>
            </Row>
          </div>
          <div className="resultBox">
            {result.length === 0 ? (
              <div className="loading">
                <Spin tip="加载中..."></Spin>
              </div>
            ) : (
              <Row>
                {result.map((item, index) => {
                  return (
                    <Col
                      key={index}
                      xs={24}
                      sm={6}
                      md={8}
                      lg={8}
                      xl={8}
                      onClick={() => {
                        this.showModel(item);
                      }}
                    >
                      <BookCard dataSource={item.Book}></BookCard>
                    </Col>
                  );
                })}
              </Row>
            )}
            <Pagination
              defaultCurrent={this.state.pageIndex}
              defaultPageSize={this.state.pageSize}
              total={books.length}
              onChange={this.pageChange}
            />
          </div>
        </div>
        <Detail
          ref={(ref) => {
            this.model = ref;
          }}
        ></Detail>
      </div>
    );
  }
}

export default HomePage;
