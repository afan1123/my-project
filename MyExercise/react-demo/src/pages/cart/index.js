import React, {Component, Fragment} from "react";
import {FormCell} from "components";
import "./index.less";
const baseData = [
  {type: "input", labelVal: "中介业务员工号", required: true},
  {type: "input", labelVal: "中介业务员姓名", required: true},
  {type: "input", labelVal: "手机号", required: true},
  {type: "select", labelVal: "证件类型", required: true},
  {type: "input", labelVal: "证件号码", required: true},
  {type: "range", labelVal: "证件有效期", required: true},
  {type: "date", labelVal: "出生日期", required: true},
  {type: "select", labelVal: "性别", required: true},
  {type: "select", labelVal: "学历", required: false},
  {type: "input", labelVal: "通讯地址", required: true},
  {type: "input", labelVal: "详细地址", required: true},
  {type: "input", labelVal: "邮编", required: false},
  {type: "select", labelVal: "民族", required: true},
  {type: "date", labelVal: "入职日期", required: true},
  {type: "space", labelVal: "", required: false},
  {type: "input", labelVal: "紧急联系人姓名", required: false},
  {type: "input", labelVal: "紧急联系人电话", required: false},
  {type: "select", labelVal: "与业务中介员关系", required: false},
];
const govermentData = [
  {type: "input", labelVal: "业务中介员状态", required: true},
  {type: "date", labelVal: "离职日期", required: true},
  {type: "input", labelVal: "分支机构全称", required: true},
];
class CartPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="right-wrap">
        <h2 className="cartTitle">基础信息</h2>
        <FormCell formData={baseData}></FormCell>
        <h2 className="cartTitle">行政信息</h2>
        <FormCell formData={govermentData}></FormCell>
        <h2 className="cartTitle">证书信息</h2>
      </div>
    );
  }
}

export default CartPage;
