import React, {Component} from "react";
import LoginForm from "components/loginform";
import loginBg from "assets/images/loginBg.png";
import "./index.css";
class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {currentUser: null};
  }
  /*
    功能：登录，前往首页
  */
  login() {
    this.props.history.push("/welcome");
  }
  /*
    功能：当其他页面回到登录页，加载当前的账号密码
  */
  backLogin = () => {
    let localUser = JSON.parse(localStorage.getItem("user"));
    if (localUser !== null) {
      this.setState({currentUser: localUser});
    }
  };
  componentDidMount() {
    this.backLogin();
  }
  render() {
    return (
      <div className="container">
        <div className="loginLeft">
          <img src={loginBg} alt="" />
        </div>
        <div className="loginRight">
          <LoginForm
            currentUser={this.state.currentUser} //当前用户信息
            backFn={() => this.login()} //登录操作
          ></LoginForm>
        </div>
      </div>
    );
  }
}

export default LoginPage;
