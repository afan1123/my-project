import React, {Component, Fragment} from "react";
import {Card} from "antd";
const {Meta} = Card;
import "./card.css";
import {
  HomeFilled,
  UnorderedListOutlined,
  ShoppingCartOutlined,
  UserOutlined,
} from "@ant-design/icons";
import styled from "styled-components";

const style = {fontWeight: "bold", color: "#fff", fontSize: "70px"};

const Home = styled(HomeFilled)`
  ${style},
  .ant-card-cover {
    padding: 20px 0;
    background-color: #1890ff;
  }
`;
const List = styled(UnorderedListOutlined)`
  ${style},
  .ant-card-cover {
    padding: 20px 0;
    background-color: #1890ff;
  }
`;
const Cart = styled(ShoppingCartOutlined)`
  ${style},
  .ant-card-cover {
    padding: 20px 0;
    background-color: #1890ff;
  }
`;
const Mine = styled(UserOutlined)`
  ${style},
  .ant-card-cover {
    padding: 20px 0;
    background-color: #1890ff;
  }
`;
class Icon extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    if (this.props.title == "首页") {
      return <Home />;
      // return <CardStyle>111</CardStyle>;
    }
    if (this.props.title == "列表") {
      return <List />;
    }
    if (this.props.title == "购物车") {
      return <Cart />;
    } else {
      return <Mine />;
    }
  }
}

class CardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Fragment>
        <Card
          hoverable
          style={{width: 240}}
          cover={<Icon title={this.props.title}></Icon>}
        >
          <Meta title={this.props.title} />
        </Card>
      </Fragment>
    );
  }
}

export default CardComponent;
