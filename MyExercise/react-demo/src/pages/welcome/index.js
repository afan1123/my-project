import React, {Component} from "react";
import {PageHeader, Row, Col} from "antd";
import Card from "./card";
import style from "./index.css";
const pd8 = {padding: "8px"};
class WelcomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div>
        {/* <PageHeader className={style['site-page-header']} title="欢迎页" /> */}
        <div className="right-wrap">
          <Row gutter={16}>
            <Col className="gutter-row" span={6}>
              <div style={pd8}>
                <Card title="首页"></Card>
              </div>
            </Col>
            <Col className="gutter-row" span={6}>
              <div style={pd8}>
                <Card title="列表"></Card>
              </div>
            </Col>
            <Col className="gutter-row" span={6}>
              <div style={pd8}>
                <Card title="购物车"></Card>
              </div>
            </Col>
            <Col className="gutter-row" span={6}>
              <div style={pd8}>
                <Card title="我的"></Card>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default WelcomePage;
