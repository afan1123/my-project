import React, {Component, Fragment} from "react";
import SlideBar from "components/slidebar";
import {Route, Redirect} from "react-router-dom";
import WelcomePage from "../welcome";
import HomePage from "../home";
import CartPage from "../cart";
import ListPage from "../list";
import MinePage from "../mine";
import {Newpageheader, Header} from "components";
import "./index.less";
const titleList = {
  "/welcome": "欢迎页",
  "/list": "列表页",
  "/home": "首页",
  "/cart": "购物侧",
};
class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      title: "欢迎页",
    };
  }
  toPage(page) {
    this.setState({title: titleList[page]});
    this.props.history.push(page);
  }
  componentDidMount() {
    this.toPage(this.props.location.pathname);
  }

  render() {
    return (
      <Fragment>
        <Redirect to="/login"></Redirect>
        <Header title={this.state.title}></Header>
        <div className={"mainContainer"}>
          <div style={{width: 256, height: "100%"}}>
            <SlideBar
              toPage={(page) => {
                this.toPage(page);
              }}
              title={this.props.location.pathname}
            ></SlideBar>
          </div>
          <div className={"mainRight"}>
            <Newpageheader history={this.state.title}></Newpageheader>
            <Route path="/" exact component={WelcomePage}></Route>
            <Route path="/welcome" component={WelcomePage}></Route>
            <Route path="/home" component={HomePage}></Route>
            <Route path="/list" component={ListPage}></Route>
            <Route path="/cart" component={CartPage}></Route>
            <Route path="/mine" component={MinePage}></Route>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default MainPage;
