import React from "react";
import "./index.css";
import {Row, Col} from "antd";
export function EditUi() {
  return (
    <div className="editContainer">
      <h2>基础信息</h2>
      <Row>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              被保人证件号<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              中介业务员姓名<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              手机号<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              证件类型<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              证件号码<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              证件有效期<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              出生日期<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              性别<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">学历</label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              通讯地址<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              详细地址<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">邮编</label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              民族<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">
              入职日期<span>*</span>
            </label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}></Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">紧急通讯联系人姓名</label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">紧急通讯联系人电话</label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">与中介业务员关系</label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="inputCell">
            <label htmlFor="insuredIdentify">与中介业务员关系</label>
            <input type="text" id="insuredIdentify" placeholder="请输入..." />
          </div>
        </Col>
      </Row>
    </div>
  );
}
