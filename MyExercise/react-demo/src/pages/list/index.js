// 依赖
import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
// 组件
import {message} from "antd";
import {Newtable, SearchBox} from "components";
// 仓库
import store from "store";
import {getCategoryAction} from "store/actionCreators";
// 资源
import {Iconfont} from "assets/iconfonts";
// 服务
import category from "service/category";

class ListPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      info: {},
    };
  }
  // 获取列表
  async getList() {
    try {
      return await category.getList();
    } catch (error) {
      console.log(error);
    }
  }
  // 模拟删除
  confirm() {
    message.success("删除成功");
  }
  //
  setList = () => {
    this.setState({list: store.getState().data, info: store.getState().info});
  };

  componentDidMount() {
    this.props.getCategory();
  }

  render() {
    return (
      <div>
        <div className="right-wrap">
          <SearchBox info={this.state.info}></SearchBox>
          <div
            className="outputExcel"
            style={{padding: "10px 0", fontSize: "18px"}}
          >
            <Iconfont type="icon-Excel" style={{fontSize: "25px"}}></Iconfont>{" "}
            导出Excel
          </div>
          <Newtable data={this.props.data} confirm={this.confirm}></Newtable>
        </div>
      </div>
    );
  }
}
const stateToProps = (state) => {
  return {
    data: state.data,
  };
};

const dispatchToProps = (dispatch) => {
  return {
    getCategory() {
      const action = getCategoryAction();
      dispatch(action);
    },
  };
};

export default connect(stateToProps, dispatchToProps)(ListPage);
