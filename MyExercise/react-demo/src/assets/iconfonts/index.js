import {createFromIconfontCN} from "@ant-design/icons";

export const Iconfont = createFromIconfontCN({
  scriptUrl: "//at.alicdn.com/t/font_2323095_6icsbs4tesy.js",
});
