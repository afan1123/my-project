### 创建 develop

# 在本地创建分支

git branch develop

# 将分支推送到版本仓库

git push -u origin develop

### feature 阶段

## 开始

# 通过 develop 新建 feature 分支

git checkout -b feature develop
git push -u origin feature
...

# 开发完成后提交

# 查看文件在工作区、暂存区、仓库的状态

git status
git add .
git commit

## 完成

# 拉 develop 分支

git pull orign develop

# 切换分支

git checkout develop

# --no-ff：不使用 fast-forward 方式合并，保留分支的 commit 历史

# --squash：使用 squash 方式合并，把多次分支 commit 历史压缩为一次

# 合并分支，并保留分支 commit 版本

git merge --no-ff feature

# 推送分支

git push origin develop

# 删除合并的分支

git branch -d some-feature

# 如果需要删除远程 feature 分支:

git push origin --delete feature

### Release 阶段

## 开始

# 通过 develop 分支创建 release 分支

git checkout -b release-0.1.0 develop

## 完成

# 切换回主分支

git checkout master

# 合并分支并保留 commit 版本

git merge --no-ff release-0.1.0

# 推送 master

git push

# 切换回 develop 分支

git checkout develop

# 合并 develop 上的分支

git merge --no-ff release-0.1.0

# 推送 develop 分支

git push

# 删除 release 分支

git branch -d release-0.1.0

# 删除远程 release 分支

git push origin --delete release-0.1.0

# 合并 master/devlop 分支之后，打上 tag

git tag -a v0.1.0 master
git push --tags

### Hotfix 阶段

## 开始

# 通过 master 创建 hotfix 分支

git checkout -b hotfix-0.1.1 master

## 完成

# 切换回主分支

git checkout master

# 合并并保存分支 commit 版本

git merge --no-ff hotfix-0.1.1

# 推送

git push

# 切换回 develop 分支

git checkout develop

# 合并并保留分支 commit 版本

git merge --no-ff hotfix-0.1.1

# 推送分支

git push

# 删除本地 hotfix 分支

git branch -d hotfix-0.1.1

# 删除远程仓库 hotfix 分支

git push origin --delete hotfix-0.1.1

# 合并分支打上 tag

git tag -a v0.1.1 master
git push --tags
