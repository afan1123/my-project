//webpackage.config.js

const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const webpack = require("webpack"); //增加导入webpack
const values = require("postcss-modules-values");
module.exports = {
  mode: "development",
  devtool: "cheap-module-source-map",
  devServer: {
    contentBase: path.join(__dirname, "./src/"),
    publicPath: "/",
    host: "127.0.0.1",
    port: 3000,
    stats: {
      colors: true,
    },
    proxy: {
      "/api": {
        target: "http://139.9.209.237/librarywebapi/",
        changeOrigin: true,
        pathRewrite: {"^/api": ""},
      },
    },
    hot: true, //在devServer中增加hot字段
    historyApiFallback: true,
  },
  entry: ["./src/index.js", "./src/dev.js"], //在entry字段中添加触发文件配置
  // 将 jsx 添加到默认扩展名中，省略 jsx
  resolve: {
    extensions: [".wasm", ".mjs", ".js", ".json", ".jsx"],
    alias: {
      assets: path.resolve(__dirname, "src/assets"),
      store: path.resolve(__dirname, "src/store"),
      pages: path.resolve(__dirname, "src/pages"),
      components: path.resolve(__dirname, "src/components"),
      css: path.resolve(__dirname, "src/css"),
      images: path.resolve(__dirname, "src/images"),
      service: path.resolve(__dirname, "src/service"),
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
            plugins: ["@babel/plugin-proposal-class-properties"],
          },
        },
      },
      {
        test: /\.(css|less)$/,
        exclude: /(node_modules|commonCss)/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            // options: {
            //   modules: {
            //     localIdentName: "[path][name]---[local]---[hash:base64:5]",
            //   },
            // },
          },
          "postcss-loader",
          {
            loader: "less-loader",
            options: {
              lessOptions: {
                javascriptEnabled: true,
              },
            },
          },
        ],
        // loader: "style-loader!css-loader?modules!postcss-loader",
      },
      {
        test: /\.(css|less)$/,
        include: /(node_modules|commonCss)/,
        use: [
          "style-loader",
          "css-loader",
          "postcss-loader",
          {
            loader: "less-loader",
            options: {
              lessOptions: {
                javascriptEnabled: true,
              },
            },
          },
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"],
      },
    ],
  },
  plugins: [
    // plugins中增加下面内容，实例化热加载插件
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebPackPlugin({
      template: "public/index.html",
      filename: "index.html",
      inject: true,
    }),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [values],
      },
    }),
  ],
};
